# cls

Tool for navigating directories and viewing files.


--- 


## Easy Autocompletion

cls uses native autocompletion to present possible file paths and files of all extensions.

## Easy Viewing

cls uses bat as a its file viewer.