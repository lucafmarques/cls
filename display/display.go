package display

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func visit(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatalf("Couldn't walk file. {err}")
		}
		if strings.Contains(path, ".git") {
			return nil
		}
		*files = append(*files, path)
		return nil
	}
}

func Travese(root string) {
	var files []string

	err := filepath.Walk(root, visit(&files))
	if err != nil {
		log.Panicf("Failed traversing files. {err}")
	}

	for _, file := range files {
		fmt.Println(file)
	}
}
